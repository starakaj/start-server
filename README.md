# staRt server side app #

[![Build Status](https://drone.io/bitbucket.org/aquincum/start-server/status.png)](https://drone.io/bitbucket.org/aquincum/start-server/latest)
[![codecov.io](https://codecov.io/bitbucket/aquincum/start-server/coverage.svg?branch=master)](https://codecov.io/bitbucket/aquincum/start-server?branch=master)

more to come..

## Password Protected API
The staRt API is password protected by nginx, therefore, you'll need to include a `.env` file in the root of this directory which will not be checked into git. The structure of this file is as follows:
```bash
API_USERNAME=<username>
API_PASSWORD=<password>
```
Note that you will need to make sure this file is present locally, as well as on the production server! If you don't know what the username and password are, just ask. Then, when you build the webpack, these variables will automatically get bundled.
