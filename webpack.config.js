var path = require("path");
var webpack = require("webpack");
require('dotenv').config()


var dir = function(d){
    return path.join(__dirname, d ? d : "");
};

var config =  {
    entry: "./app.js",
    output: {
        filename: "bundle.js",
        path: dir("client/dist/"),
        publicPath: "dist/"
    },
    context: dir("client/js"),
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'API_USERNAME': '"' + process.env.API_USERNAME + '"',
                'API_PASSWORD': '"' + process.env.API_PASSWORD + '"',
            }
        })
    ],
    module: {
	loaders: [
	    {test: /\.css$/, loader: 'style!css'},
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
            { test: /\.woff2$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file"},
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" }
	]
    }
};


if (process.env.NODE_ENV === 'test') {
	config.entry = './test.js';
	config.context = dir('test/client-side/');
}


module.exports = config;
