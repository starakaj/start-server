var assert = require("assert");
var fs = require("fs");
var database = require("../server/database");
var GridStore = require("mongodb").GridStore;

describe("Database", function(){
    it("Should connect to the database", function(){
        return database.mongoConnect();
    });
});


describe("GridStore", function(){
    var fn = "temptestfile.txt";
    var contents = "Hello world! I am testing.";
    var fileId;
    it("Should be able to save file", function(){
        fs.writeFileSync(fn, contents);
        return database.gridSaveFile(database.db(), fn).then(function(fid){
            fileId = fid;
            GridStore.exist(database.db(), fileId).then(function(exists){
                assert.equal(exists, true);
            });
        });
    });
    it("Should be able to retrieve file", function(){
        return database.gridReadFileStream(database.db(), fileId).then(function(str){
            var buf = "";
            str.on("data", function(ch){
                buf += ch;
            });
            str.on("end", function(){
                assert.equal(buf, contents);
            });
        });
    });
    it("Should clean up", function(){
        fs.unlinkSync(fn);
        return new GridStore(database.db(), fileId, "r").open().then(function(gs){
            gs.unlink();
        });
    });
});
