require('dotenv').config();
var assert = require("assert");
var request = require("supertest");
var fs = require("fs");
var mongo = require("mongodb");
var Promise = require("bluebird");
var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: process.env.AWS_CLIENT,
    secretAccessKey: process.env.AWS_SECRET
});
var s3 = Promise.promisifyAll(new AWS.S3());
var moment = require("moment");
var Session = require("../server/models/session");
var database = require("../server/database");
var server;
var audioFN = "test/ttt-2015-12-08T17-22-52-audio.m4a";
var lpcFN = "test/ttt-2015-12-08T17-22-52-lpc.csv";
var metaFN = "test/ttt-2015-12-08T17-22-52-meta.csv";
var ratingsFN = "test/ttt-2015-12-08T17-22-52-ratings.json";
var audioData;
var audioAltData;
var lpcData;
var metaDataData;
var metaDataFields;
var my_id;

before("Reading test files", function(){
    rawAudioData = fs.readFileSync(audioFN, "binary");
    audioData = fs.readFileSync("test/ttt-2015-12-08T17-22-52-audio.flac");
    audioAltData = fs.readFileSync("test/ttt-2015-12-08T17-22-52-audio-alt.flac");
    lpcData = fs.readFileSync(lpcFN);
    ratingsData = fs.readFileSync(ratingsFN);
    metaDataData = fs.readFileSync(metaFN);
    metaDataFields = metaDataData.toString().split("\n")[1].split(", ");
    metaDataFields[1] = parseInt(metaDataFields[1].replace(/[^0-9\.]/,""));
    metaDataFields[12] = moment(metaDataFields[12],"YYYY-MM-DDTHH-mm-ss").toDate();
});

before("Connecting to the server", function(){
    server = require("../index");
});

describe("Getting version", function(){
    it("Should work", function(done){
        request(server)
            .get("/version")
            .expect(200)
            .expect(require("../package.json").version, done);
    });
});

describe("Checking session", function() {
    it ("Should work", function(done) {
        var s = new Session(1);
        console.log(s);
        done();
    })
});

describe("Uploading session piece by piece", function() {
    it("Should upload metadata", function(done){
        request(server)
            .post("/session/simple/metadata?session_id=432")
            .set("fileName", "metafile.csv")
            .send(metaDataData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload lpc", function(done){
        request(server)
            .post("/session/simple/lpc?session_id=432")
            .set("fileName", "lpc.csv")
            .send(lpcData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload audio", function(done){
        request(server)
            .post("/session/simple/audio?session_id=432")
            .set("fileName", "audio.m4a")
            .send(rawAudioData.toString())
             .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should upload ratings", function(done){
        request(server)
            .post("/session/simple/ratings?session_id=432")
            .set("fileName", "ratings.csv")
            .send(ratingsData.toString())
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Session should be there", function() {
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(db){
                return mongo.GridStore.list(db);
            }).then(function(files){
                assert.equal(files.length, 3);
            });
    });
    it("Content should be correct", function(){
        var db;
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db){
                db = _db;
                return db.collection("sessions").findOne({session_id: "432"});
            })
            .then(function(doc){
                console.log("D", doc);
                assert.ok(doc.session_id);
                return Promise.all([
                    mongo.GridStore.read(db, doc.audioDataID),
                    mongo.GridStore.read(db, doc.lpcDataID),
                    mongo.GridStore.read(db, doc.ratingsID),
                    Promise.resolve(doc)
                ]);
            })
            .then(function(datas){
                if(datas[0].toString() != audioData.toString()){
                    assert.deepEqual(datas[0].toString().length, audioData.toString().length);
                }
                assert.deepEqual(datas[1], lpcData);
                assert.deepEqual(datas[2], ratingsData);
                assert.equal(datas[3].metadata.stream_sample_rate, metaDataFields[1]);
                assert.equal(datas[3].metadata.username, metaDataFields[4]);
                assert.equal(datas[3].metadata.deviceID, metaDataFields[3]);
                assert.deepEqual(datas[3].metadata.start_date, metaDataFields[12]);
            });
    });
});


describe("Uploading one by one, using multipart form", function(){
    it("Should work based on metadata test", function(done){
        request(server).post("/session/metadata?session_id=433")
            .field("session_id", "433")
            .attach("file", metaFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on lpc test", function(done){
        request(server).post("/session/lpc?session_id=433")
            .field("session_id", "433")
            .attach("file", lpcFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on audio test", function(done){
        request(server).post("/session/audio?session_id=433")
            .field("session_id", "433")
            .attach("file", audioFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });
    it("Should work based on ratings test", function(done){
        request(server).post("/session/ratings?session_id=433")
            .field("session_id", "433")
            .attach("file", ratingsFN)
            .expect(200)
            .expect(function(res){
                assert.equal(JSON.parse(res.text).success, true);
            }, done)
            .end(done);
    });

});


describe("Moving to AWS", function() {
    var targetId = "433";
    it("Should have the original session", function() {
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db){
                db = _db;
                return db.collection("sessions").findOne({session_id: "433"});
            })
            .then(function(doc){
                console.log("D", doc);
                assert.ok(doc.session_id);
                assert.equal(doc.awsManaged, false);
                // return Promise.all([
                //     mongo.GridStore.read(db, doc.audioDataID),
                //     mongo.GridStore.read(db, doc.lpcDataID),
                //     mongo.GridStore.read(db, doc.ratingsID),
                //     Promise.resolve(doc)
                // ]);
            });
    });
    it("Should include the original session amound grid managed sessions", function(done) {
        Session.listAllGridManaged().then(function (res) {
            var sessionId = null;
            res.forEach(function (doc) {
                if (doc.session_id === targetId) sessionId = doc.session_id;
            });
            assert.equal(sessionId, targetId);
            done();
        });
    });
    it("Should set keys for AWS upload", function() {
        this.timeout(60000);
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db) {
                db = _db;
                return database.transferToAWS(db, "433");
            })
            .then(function () {
                var targetId = "433";
                return db.collection("sessions").findOne({ session_id: targetId });
            })
            .then(function (doc) {
                assert.ok(doc.session_id);
                assert.equal(doc.awsManaged, true);
                assert.ok(doc.audioDataAWSKey);
                assert.ok(doc.lpcDataAWSKey);
                assert.ok(doc.ratingsDataAWSKey);
            });
    });
    it("Should make files available from AWS", function() {
        this.timeout(60000);
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db) {
                db = _db;
                return db.collection("sessions").findOne({ session_id: targetId });
            }).then(function (doc) {
                return Promise.all([
                    s3.getObject({
                        Bucket: process.env.AWS_BUCKET,
                        Key: doc.audioDataAWSKey
                    }).promise(),
                    s3.getObject({
                        Bucket: process.env.AWS_BUCKET,
                        Key: doc.lpcDataAWSKey
                    }).promise(),
                    s3.getObject({
                        Bucket: process.env.AWS_BUCKET,
                        Key: doc.ratingsDataAWSKey
                    }).promise(),
                    Promise.resolve(doc)
                ]);
            }).then(function (data) {
                return Promise.all([
                    Promise.resolve(data[0].Body),
                    Promise.resolve(data[1].Body),
                    Promise.resolve(data[2].Body),
                    Promise.resolve(data[3])
                ]);
            }).then(function(datas){
                debugger;
                if(datas[0].toString() != audioData.toString()){
                    assert.deepEqual(datas[0].toString().length, audioData.toString().length);
                }
                assert.deepEqual(datas[1], lpcData);
                assert.deepEqual(datas[2], ratingsData);
                assert.equal(datas[3].metadata.stream_sample_rate, metaDataFields[1]);
                assert.equal(datas[3].metadata.username, metaDataFields[4]);
                assert.equal(datas[3].metadata.deviceID, metaDataFields[3]);
                assert.deepEqual(datas[3].metadata.start_date, metaDataFields[12]);
            });
    });
    it("Should delete the original GridStore files", function() {
        return mongo.MongoClient.connect(require("../server/database").url)
            .then(function(_db) {
                db = _db;
                return db.collection("sessions").findOne({ session_id: targetId });
            }).then(function (doc) {
                return Promise.all([
                    mongo.GridStore.exist(db, doc.lpcDataID),
                    mongo.GridStore.exist(db, doc.audioDataId),
                    mongo.GridStore.exist(db, doc.ratingsId)
                ]);
            }).then(function (results) {
                assert.equal(results[0], false);
                assert.equal(results[1], false);
                assert.equal(results[2], false);
            });
    });
});

describe("Getting session", function(){
    var my_id = "433";
    it("Metadata should be retrieved unharmed", function(done){
        request(server)
            .get("/session?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                assert.equal(res.body.session_id, my_id);
                assert.equal(res.body.metadata.stream_sample_rate, metaDataFields[1]);
                assert.equal(res.body.metadata.username, metaDataFields[4]);
                assert.equal(res.body.metadata.deviceID, metaDataFields[3]);
                assert.deepEqual(moment(res.body.metadata.start_date).toDate(), metaDataFields[12]);
            })
            .end(done);
    });
    it("Should tell me if session doesn't exist", function(done){
        request(server)
            .get("/session?session_id=4u0985432527435723")
            .expect(404)
            .expect({error: "No such session found"}, done);
    });
    it("Downloaded LPC file should be retrieved unharmed", function(done){
        this.timeout(5000);
        request(server).get("/session/lpc?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                assert.equal(res.text, lpcData.toString());
            })
            .end(done);
    });
    it("Downloaded audio file should be retrieved unharmed", function(done){
        this.timeout(5000);
        request(server).get("/session/audio?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                if(res.text != audioData.toString()){
                    assert.deepEqual(res.text.length, audioData.toString().length);
                }
                else{
                    assert.equal(1, 1);
                }
            })
            .end(done);
    });
    it("Downloaded ratings file should be retrieved unharmed", function(done){
        request(server).get("/session/ratings?session_id=" + my_id)
            .expect(200)
            .expect(function(res){
                assert.equal(res.text, ratingsData.toString());
            })
            .end(done);
    });
    it("Should list the session(s)", function(done){
        request(server)
            .get("/session")
            .expect(200)
            .expect(function(res){
                var data = JSON.parse(res.text);
                assert.equal(data.length, 2);
                assert.equal(data[0].metadata.username, metaDataFields[4]);
            })
            .end(done);
    });
});


after("Remove Grid and session", function(){
    var db;
    return mongo.MongoClient.connect(require("../server/database").url)
        .then(function(_db){
            db = _db;
            return db.collection("fs.files").find().toArray();
        }).then(function(names){
            return Promise.all(names.map(function(name){
                return mongo.GridStore.unlink(db, name._id);
            }));
        }).then(function(){
            db.collection("sessions").remove();
        });
});


