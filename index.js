var argv = require("yargs").usage("Usage: $0 [options]")
    .alias("p", "port")
    .nargs("p", 1)
    .describe("p", "Port number to run on. By default, the PORT environment variable will be used, or if that is not set, the default is 3000.")
    .string("p")
    .help("h")
    .alias("h", "help")
    .describe("h", "Print this help screen")
    .argv;

require('dotenv').config()
var server = require("./server/server");
var port = argv.p || process.env.PORT || 3000;

var database = require("./server/database");

database.mongoConnect().then(function(db) {
    database.setDB(db);
    console.log("Database open");
}).catch(function(err){
    console.log("Could not open connection to database: " + err);
    process.exit(1);
});

module.exports = server.listen(parseInt(port, 10), function(err, res){
    if(err){
        console.log("Error starting server: " + err);
    }
    else{
        console.log("Listening on " + port + ".");
    }
});

process.on("exit", function(){
    var db = database.db();
    if(db){
        db.close();
        console.log("Database closed");
    }
});
