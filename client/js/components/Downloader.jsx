import React from 'react';
import {Button,Glyphicon} from 'react-bootstrap';
//import {saveAs} from 'file-saver';
//import {createWriteStream} from 'streamsaver'
//import 'whatwg-fetch';


class Downloader extends React.Component {
    render(){
        return (
            <Button bsStyle="primary" onClick={this.handleClick.bind(this)}>
                Download <Glyphicon glyph="download-alt" ></Glyphicon>
            </Button>
            );
    }

    download(filename, text) {
        // from http://stackoverflow.com/a/18197511/680464
        var pom = document.createElement('a');
        var mime = this.props.type == "audio" ? "audio/flac" : "text/csv";
        pom.setAttribute('href', 'data:'+mime+';charset=utf-8,' + encodeURIComponent(text));
        pom.setAttribute('download', filename);
        
        document.body.appendChild(pom);
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        }
        else {
            pom.click();
        }
        document.body.removeChild(pom);
    }
    handleClick(){
        var win = window.open("session/" + this.props.type + "?session_id=" + this.props.session, '_blank');
        win.focus();
        // var extension = this.props.type == "audio" ? "flac" : "csv";
        // fetch("session/" + this.props.type + "?session_id=" + this.props.session, {
        //     headers: {
        //         'Authorization': 'Basic ' + btoa(process.env.API_USERNAME + ':' + process.env.API_PASSWORD)
        //     }
        // })
        //     .then(response => response.text())
        //     .then(txt => this.download(`${this.props.type}${this.props.session}.${extension}`, txt))
        //     .catch(e => console.error(`Error: ${e} `))
    }
}

Downloader.propTypes = {
    session: React.PropTypes.string,
    type: React.PropTypes.string
}

export default Downloader;
