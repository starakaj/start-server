import React from 'react';
import {Grid, Col, PageHeader} from 'react-bootstrap';
import SoundListContainer from './SoundListContainer';

class App extends React.Component {
    render() {
        return (
            <Grid>
                <Col xs={12} md={12} lg={10} lgOffset={1}>
                    <PageHeader>
                        staRt <small>server administration -- development version</small>
                    </PageHeader>
                    <SoundListContainer></SoundListContainer>
                </Col>
            </Grid>
        )
    }
}

export default App;
