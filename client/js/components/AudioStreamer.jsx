// ABORTED, as FLAC is an issue :/
// LATER TODO: convert to MP3 on server side and stream that?

import React from 'react';
import {Button, Glyphicon} from 'react-bootstrap';

class AudioStreamer extends React.Component {
    render(){
        var audioSrc = `/session/audio?session_id=${this.props.session}`
        return (
            <Button bsStyle="info">
                Play
                <audio preload="none" src={audioSrc}>
                </audio>
                <Glyphicon glyph="play">
                </Glyphicon>
            </Button>
        )
    }
}


AudioStreamer.propTypes = {
    session: React.PropTypes.string
}

export default AudioStreamer;
