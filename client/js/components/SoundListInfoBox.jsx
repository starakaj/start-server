import React from 'react';
import {Alert} from 'react-bootstrap';

class SoundListInfoBox extends React.Component {
    render(){
        return (
            <Alert bsStyle={this.props.errStyle}>
                {this.props.msg}
            </Alert>
        )
    }
}

export default SoundListInfoBox;
