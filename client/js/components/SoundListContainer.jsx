import React from 'react';
import SoundList from './SoundList';
import SoundListInfoBox from './SoundListInfoBox';
import 'whatwg-fetch';

class SoundListContainer extends React.Component {
    constructor(props){
        super(props);
        this.state = {entries: [], error: null}
    }
    getSessions(){
         fetch('session', {
            headers: {
                'Authorization': 'Basic ' + btoa(process.env.API_USERNAME + ':' + process.env.API_PASSWORD)
            }
         })
            .then(response => response.json())
            .then(data => this.setState({entries: data, error: null}))
            .catch(error => {
                console.error(error);
                this.setState({entries: [], error});
            });       
    }
    componentDidMount(){
        this.getSessions();
    } 
    render(){
        var infoBox = "";
        if(this.state.error){
            infoBox = <SoundListInfoBox errStyle="danger" msg={this.state.error}></SoundListInfoBox>
        }
        else if(this.state.entries.length === 0){
            infoBox = <SoundListInfoBox errStyle="warning" msg={"No entries in the database"}></SoundListInfoBox>
        }
        else {
            var soundList = <SoundList entries={this.state.entries}/>;
        }
        return (
            <div>
                {infoBox}
                {soundList}
            </div>
            
        )
        
    }
}

export default SoundListContainer;