import React from 'react';
import SoundFile from './SoundFile';
import {Table} from 'react-bootstrap';
import moment from 'moment';

class SoundList extends React.Component {
    render(){
        const sortedEntries = this.props.entries.slice(0).sort((a, b) => {
            if (a.metadata && b.metadata) {
                const dateA = moment(a.metadata.start_date);
                const dateB = moment(b.metadata.start_date);
                return dateB.diff(dateA);
            } else if (a.metadata) {
                return -1;
            } else if (b.metadata) {
                return 1;
            } else {
                return 0;
            }
        });
        const entryLines = sortedEntries.map((entry, ix) => <SoundFile data={entry} key={ix}></SoundFile>);
        
        return(
            <Table striped>
                <thead><tr>
                    <th>username</th>
                    <th>record date</th>
                    <th>device ID</th>
                    <th>session ID</th>
                    <th>audio</th>
                    <th>lpc</th>
                    <th>ratings</th>
                    <th>storage</th>
                    <th></th>
                </tr></thead>
                <tbody>
                    {entryLines}
                </tbody>
            </Table>
        )
    }
    
}

export default SoundList;
