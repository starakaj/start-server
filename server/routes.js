var multer = require("multer");
var upload = multer({dest: "uploads/"});
//var bodyParser = require("body-parser");

// Controllers
var version = require("./controllers/version.js");
var prompt = require("./controllers/prompt.js");
var uploadsession = require("./controllers/uploadsession.js");
var getsession = require("./controllers/getsession.js");
/*var bodyMiddleware = bodyParser({
    limit: "100MB",
    type: "application/octet-stream"
    });*/

var bodyMiddleware = function(req, res, next){
    req.body = '';
    req.setEncoding('utf-8');

    req.on('data', function(chunk) { 
        req.body += chunk;
    });

    req.on('end', function() {
        next();
    });
}

/**
 * Routes set up here. Require the controllers above
 */
module.exports = function(app){
    app.get("/version", version);
    app.post("/session", upload.fields([
        {name: "audioData", maxCount: 1},
        {name: "lpcData", maxCount: 1},
        {name: "metadataData", maxCount: 1}
    ]), uploadsession.whole);
    app.post("/prompt", prompt.get);
    app.get("/session", getsession.get);
    app.get("/session/metadata", getsession.get); // synonym for /session
    app.get("/session/lpc", getsession.downloadFile.bind(null, "lpc"));
    app.get("/session/audio", getsession.downloadFile.bind(null, "audio"));
    app.get("/session/ratings", getsession.downloadFile.bind(null, "ratings"));
    app.post("/session/metadata", upload.any(), uploadsession.mfMetadata);
    app.post("/session/lpc", upload.any(), uploadsession.mfField.bind(null, "text/csv", "uploadLPC"));
    app.post("/session/audio", upload.any(), uploadsession.mfField.bind(null, "audio/mp4", "uploadAudio"));
    app.post("/session/ratings", upload.any(), uploadsession.mfField.bind(null, "application/json", "uploadRatings"));
    app.post("/session/simple/metadata", bodyMiddleware, uploadsession.metadata);
    app.post("/session/simple/lpc", bodyMiddleware, uploadsession.uploadLPC);
    app.post("/session/simple/audio", bodyMiddleware, uploadsession.uploadAudio);
    app.post("/session/simple/ratings", bodyMiddleware, uploadsession.uploadRatings);
};
