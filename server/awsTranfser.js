/**
 * Run this script in a cron (every hour for example) to convert database entries from GridStore to AWS
 */

 // Iterate over the entries in the database
 // For each one that's in gridstore
 // Send it to AWS
 // Convert the entry from GridStore to AWS

require('dotenv').config()
var database = require("../server/database");
var Session = require("./models/session");

database.mongoConnect().then(function(db) {
    database.setDB(db);
    console.log("Database open");
    doTransfer().then(() => {
        console.log("All GridStore sessions transferred successfully to AWS");
        process.exit(0);
    });
}).catch(function(err){
    console.log("Could not open connection to database: " + err);
    process.exit(1);
});

function transferSession(doc) {
    return database.transferToAWS(database.db(), doc.session_id)
        .catch(function (err) {
            console.log(`Enountered error transferring session ${doc.session_id}, skipping`);
            console.log(err);
        });
}

function printProgress(idx, total) {
    var percent = idx/total;
    var len = 40;
    var hashes = Math.floor(percent * len);
    var dashes = len - hashes;
    console.log("[" + Array(hashes).fill("#").join('') + Array(dashes).fill(".").join('') + "]");
}

async function doTransfer() {
    let transferCount = 0;
    var gridManagedSessions = await Session.listAllGridManaged();
    let totalTransfers = gridManagedSessions.length;
    console.log(`${totalTransfers} sessions to transfer to AWS`);
    for (let i=0; i<gridManagedSessions.length; i++) {
        var doc = gridManagedSessions[i];
        await transferSession(doc);
        console.log(`Transferred ${++transferCount} of ${totalTransfers}`);
        printProgress(transferCount, totalTransfers);
    }
    return Promise.resolve();
}