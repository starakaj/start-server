var Promise = require("bluebird");
var ffmpeg = require("fluent-ffmpeg");

/**
 * Audio conversion will come here.
 * @module
 */

/**
 * Returns a converted audio file.
 * @param {String} filename The returned file name
 * @returns {Promise} Promise resolved with converted file name
 */
var toStorage = module.exports.toStorage = function(filename){
    var outputFN = filename + "-conv.flac";
    return new Promise(function(resolve, reject){
        ffmpeg(filename)
            .inputFormat("mp4")
            .save(outputFN)
            .on("start", function(line){
                console.log("FFMPEG started:", line);
            })
            .on("end", function(){
                resolve(outputFN);
            })
            .on("error", function(e, stdout, stderr){
                console.log(e.message);
                reject(e);
            });
    });
};
