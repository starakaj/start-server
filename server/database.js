require('dotenv').config();
var Promise = require("bluebird");
var GridStore = require("mongodb").GridStore;
var ObjectID = module.exports.ObjectID = require("mongodb").ObjectID;
var path = require("path");

// AWS
var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: process.env.AWS_CLIENT,
    secretAccessKey: process.env.AWS_SECRET
});
var s3 = Promise.promisifyAll(new AWS.S3());

var fs = require("fs");

/**
 * Just the database settings go here, the models will deal with the
 * details. Also, wrapping it around Promises.
 * @module
 */



/**
 * Figures out the URL of the database: if we're on Amazon EBS,
 * we're connecting to the linked mongo, otherwise we're running
 * on localhost. And if we have the STARTSERVER_MONGODB_ADDR environment
 * vairable set, use that; with STARTSERVER_MONGODB_PORT or 27017 
 * by default
 * @returns String
 */
var getURL = function(){
    var addr = process.env.STARTSERVER_MONGODB_ADDR || process.env.MONGODB_PORT_27017_TCP_ADDR || "localhost",
        port = process.env.STARTSERVER_MONGODB_PORT || process.env.MONGODB_PORT_27017_TCP_PORT || 27017,
        dbname = "startapp";
    return "mongodb://" + addr + ":" + port + "/" + dbname;
};

/** The URL of the database */
module.exports.url = getURL();
/** The raw unpromisified MongoClient object */
module.exports.MongoClient = require("mongodb").MongoClient;
/** The promisified MongoClient connection */
module.exports.mongoConnect = function(){
    var mc = require("mongodb").MongoClient;
    return mc.connect(module.exports.url)
        .then(function(db){
            console.log("MongoDB connected at " + module.exports.url);
            // Polyfill promisified collection
            db.collectionP = function(coll){
                var c = db.collection(coll);
                Promise.promisifyAll(c);
                return c;
            };
            return db;
        });
};
/** This is the database, with promisified collection request */
var _db = null;
/** Plain setter */
module.exports.setDB = function(db){
    _db = db;
};
/** Plain getter */
module.exports.db = function(){
    return _db;
};

/**
 * Saves a file from the disk to AWS
 * @param db The database
 * @param fileName The file name on the disk to store
 * @returns Promise<S3Response> A promise that resolves an S3 Response
 * of the file 
 */
module.exports.awsSaveFile = function(db, fileName) {
    var readStream = fs.createReadStream(fileName);
    var fileKey = path.basename(fileName);
    var params = {
        Bucket: process.env.AWS_BUCKET,
        Key: fileKey,
        Body: readStream
    };
    return s3.upload(params, {}).promise();
}

/** 
 * Saves a file from the disk to GridStore.
 * @param db The database
 * @param fileName The file name on the disk to store
 * @returns Promise<ObjectID> A promise that resolves with the unique ID
 * of the file.
 */
module.exports.gridSaveFile = function(db, fileName){
    var fileId = new ObjectID();
    return GridStore.exist(db, fileId).then(function(exists){
        if(exists){
            return Promise.reject("Problem with unique ID.");
        }
    }).then(function(){
        var gridStore = new GridStore(db, fileId, "w");
        return gridStore.open();
    }).then(function(gridStore){
        return gridStore.writeFile(fileName);
    }).then(function(){
        return fileId;
    });
};

/**
 * Reads back a file from AWS returning a stream
 * @param db The database
 * @param {String} fileKey The unique key as stored in AWS
 * @returns Promise<stream.Readable> A readable stream
 */
module.exports.awsReadFileStream = function(db, fileKey) {
    var params = {
        Bucket: process.env.AWS_BUCKET,
        Key: fileKey
    };
    return new Promise(function(resolve, reject) {
        s3.headObject(params, function(err, data) {
            if (err) reject(err);

            resolve(s3.getObject(params).createReadStream());
        });
    });
}

/**
 * Reads back a file from GridStore returning a stream.
 * @param db The database
 * @param {ObjectID} fileID The unique ID of the file
 * @returns Promise<stream.Readable> A readable stream
 */
module.exports.gridReadFileStream = function(db, fileId){
    return GridStore.exist(db, fileId).then(function(exists){
        if(!exists){
            return Promise.reject("File does not exist");
        }
    }).then(function(){
        var gridStore = new GridStore(db, fileId, "r");
        return gridStore.open();
    }).then(function(gridStore){
        return gridStore.stream();
    });
};

function transferFileToAWS(db, doc, fileId, filePath) {
    return module.exports.gridReadFileStream(db, fileId)
        .then(function (readStream) {
            if (!readStream) return Promise.reject("Could not create read stream for GridFile");
        
            var fileKey = filePath;
            var params = {
                Bucket: process.env.AWS_BUCKET,
                Key: fileKey,
                Body: readStream
            };
            return s3.upload(params, {}).promise();
        });
}

function deleteGridStoreChunk(db, fileId) {
    var gs = new GridStore(db, fileId, "w");
    return gs.unlink();
}

/**
 * Get a object with keys to use for each of the data files
 * @param {Object} metadata The session metadata as stored in Mongo
 * @returns {Object} The object with keys
 */
function awsFileKeysFromMetadata(prefix, metadata) {
    var lpcKey = `${metadata.username}-${path.basename(metadata.lpc_file)}`;
    var ratingsKey = lpcKey.replace('-lpc.csv', '-ratings.json');
    var audioKey = `${metadata.username}-${path.basename(metadata.audio_file)}`;
    return {
        lpcKey: prefix + lpcKey,
        audioKey: prefix + audioKey,
        ratingsKey: prefix + ratingsKey
    };
}

module.exports.transferToAWS = function(db, sessionId) {
    var coll = db.collection("sessions");
    return coll.findOne({ "session_id": sessionId })
        .then(function (doc) {
            if (!doc) return Promise.reject("Session does not exist");
            if (!doc.metadata) return Promise.reject("Session is missing metadata");
            if (doc.awsManaged) return Promise.reject("Session is already on AWS");

            var awsFileKeys = awsFileKeysFromMetadata(doc.session_id + "/", doc.metadata);

            var transferTasks = [];
            if (doc.lpcDataID) transferTasks.push(transferFileToAWS(db, doc, doc.lpcDataID, awsFileKeys.lpcKey));
            if (doc.audioDataID) transferTasks.push(transferFileToAWS(db, doc, doc.audioDataID, awsFileKeys.audioKey));
            if (doc.ratingsID) transferTasks.push(transferFileToAWS(db, doc, doc.ratingsID, awsFileKeys.ratingsKey));

            return Promise.all(
                transferTasks
            ).then(function () {
                doc.lpcDataAWSKey = doc.lpcDataID ? awsFileKeys.lpcKey : null;
                doc.audioDataAWSKey = doc.audioDataID ? awsFileKeys.audioKey : null;
                doc.ratingsDataAWSKey = doc.ratingsID ? awsFileKeys.ratingsKey : null;
                doc.awsManaged = true;
                return coll.save(doc, {w: 1});
            }).then(function() {

                var deleteTasks = [];
                if (doc.lpcDataID) deleteTasks.push(deleteGridStoreChunk(db, doc.lpcDataID));
                if (doc.audioDataID) deleteTasks.push(deleteGridStoreChunk(db, doc.audioDataID));
                if (doc.ratingsID) deleteTasks.push(deleteGridStoreChunk(db, doc.ratingsID));
                return Promise.all(
                    deleteTasks
                )
            });
        });
}