var Session = require("../models/session");

var mimeMap = {
    audio: "audio/flac",
    ratings: "text/json",
    metadata: "text/csv",
    lpc: "text/csv"
};

var extensionMap = {
    audio: ".flac",
    ratings: ".json",
    metadata: ".csv",
    lpc: ".csv" 
}

/**
 * Retrieves a session metadata. Does not download the audio + LPC files,
 * that really should be a separate request.
 */
module.exports.get = function(req, res){
    var qid = req.query.session_id;
    if(!qid){
        // list sessions!
        // return res.status(400).end("Wrong request: id must be specified.");
        Session.listAll().then(function(docs){
            res.type('json');
            res.status(200).json(docs);
            res.end();
        });
    } else {
        Session.find(qid)
            .then(function(sess){
                res.status(200).json(sess.metadata);
                res.end();
            }).catch(function(err){
                if(err == 404){
                    res.status(404).json({error: "No such session found"}).end();
                }
                else {
                    res.status(500).json({error: err}).end();
                }
            });
    }
};

module.exports.downloadFile = function(file, req, res){
    var qid = req.query.session_id;
    var mimeType = mimeMap[file];
    if(!qid){
        res.status(400).end("Wrong request: id must be specified.");
    }
    Session.find(qid)
        .then(function(sess){
            return sess.download(file);
        })
        .then(function(fstream){
            res.writeHead(200, {
                "Content-Type": mimeType,
                "Content-disposition": "attachment; filename=" + file + "-" + qid + extensionMap[file]
            });
            fstream.pipe(res);
        })
        .catch(function(err){
            if(err == 404){
                res.status(404).json({error: "No such session found"}).end();
            }
            else {
                res.status(500).json({error: err}).end();
            }
        });
};
