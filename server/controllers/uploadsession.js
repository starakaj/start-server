var Session = require("../models/session");
var Promise = require("bluebird");
var fs = Promise.promisifyAll(require("fs"));
var moment = require("moment");
var path = require("path");
//var csv = require("csv-parse");

var uploadPath = "uploads/";


var clearUploads = function(path){
    fs.readdirAsync(path)
        .then(function(files){
            var unlinkPromises = files.map(function(file){
                return fs.unlinkAsync(path+file);
            });
            return Promise.all(unlinkPromises);
        }).catch(function(e){
            console.error("Problem removing upload/ files:", e);
        });

}

var getSessionId = function(req, res){
    var session_id = null;
    if(req.query && req.query.session_id) session_id = req.query.session_id;
    else if(req.header("session_id")) session_id = req.header("session_id");
    else {
        res.status(400).json({error: "session_id missing"}).end();
        return null;
    }
    return session_id;
}

/**
 * Uploads a session. Upload to this POST request using
 * multipart/form-data, with 3 files:
 * - the audio file named "audioData"
 * - the lpc file named "lpcData"
 * - the metadata file named "metadataData"
 * WON'T USE IT probably, so there's duplications
 */
module.exports.whole = function(req, res){
    var success = true;
    ["audioData", "lpcData", "metadataData"].forEach(function(d){
        if(!req.files[d] || !req.files[d][0]){
            res.status(400).json({error:"Error: " + d + " missing"}).end();
            success = false;
        }
    });
    if(!success) return;
    var session_id = getSessionId(req, res);
    if(!session_id) return;

    var audioData = req.files.audioData[0],
        lpcData = req.files.lpcData[0],
        metadataData = req.files.metadataData[0];

    /* Will we always have these types? */
    if(audioData.mimetype !== "audio/mp4"){
        return res.status(400).json({error:"Error: wrong audio mime type"}).end();
    }
    if(lpcData.mimetype !== "text/csv" || metadataData.mimetype !== "text/csv"){
        return res.status(400).json({error:"Error: wrong lpc/metadata mime type"}).end();
    }
    /*
    var metadata = parseMetadata(metadataData.path);
    */
    parseMetadata(metadataData.path)
        .then(function(metadata){
            return Session.insert({audioDataFilename:    audioData.path,
                                   lpcDataFilename:      lpcData.path,
                                   metadata: metadata,
                                   session_id: session_id
                                  });
        })
        .then(function(id){
            res.status(200).json({id: id}).end();
        }).catch(function(e){
            res.status(500).json({error: "Problem with upload: " + e}).end();
            console.log("Upload problem:", e);
        }).finally(function(){ // cleanup the upload/ folder
            clearUploads(audioData.destination);
        });
};

var parseMetadata = function(path){
    return fs.readFileAsync(path).then(function(contents){
        var lines = contents.toString().split(/\r*\n/).map(String.bind(String.prototype.trim));
        if(lines.length != 2) {
            console.log("Metadata problem! lines = " + lines + "; contents = " + contents);
            return Promise.reject("Metadata format problem. Your input has " + lines.length + " lines instead of 2.");
        }
        var fields = lines[1].split(", ");
        if(fields.length < 4){
            return Promise.reject("Invalid metadata");
        }
        var fieldmap = {},
            headerline = lines[0].split(",").map(function(s){
                return s.trim();
            });
        for(var i = 0; i < headerline.length; i++){
            if(!fields[i]) return Promise.reject("Empty field at column " + i);
            fieldmap[headerline[i]] = fields[i];
        }
        ["stream_sample_rate", "username", "deviceID", "start_date"].map(function(f){
            if(!fieldmap[f]){
                return Promise.reject(f + " not found in metadata");
            }
        });
            
        fieldmap.stream_sample_rate = parseInt(fieldmap.stream_sample_rate.replace(/[^0-9\.]/,""), 10);
        fieldmap.start_date = moment(fieldmap.start_date,["YYYY-MM-DDTHH-mm-ss",moment.ISO_8601]);
        if(!fieldmap.start_date.isValid()){
            return Promise.reject("Invalid start_date format.");
        }
        fieldmap.start_date = fieldmap.start_date.toDate();
        return fieldmap;
    });
};


var saveBody = function(req, fileName){
    return fs.writeFileAsync(fileName, req.body.toString("binary"), "binary");
}

/** Metadata upload with a single multiform request */
module.exports.mfMetadata = function(req, res){
    var session_id = getSessionId(req, res);
    if(!session_id) return;
    var metadataData = req.files.filter(function(f) {
        return f.fieldname == "file"
    })[0];
    if(metadataData.mimetype !== "text/csv"){
        return res.status(400).json({error:"Error: wrong mime type"}).end();
    }
    
    simpleUploadMetadata(metadataData.path, session_id, res);
}

module.exports.mfField = function(mime, uploadfn, req, res){
    var session_id = getSessionId(req, res);
    if(!session_id) return;
    var fileData = req.files.filter(function(f) {
        return f.fieldname == "file"
    })[0];
    if(fileData.mimetype !== mime){
        return res.status(400).json({error:"Error: wrong mime type"}).end();
    }
    
    return simpleUpload(session_id, fileData.path, uploadfn, res);
}



module.exports.metadata = function(req, res){
    var fileName = uploadPath + (req.header("filename") || "temp.tmp");
    var session_id = getSessionId(req, res);
    if(!session_id) {
        return;
    }
    
    saveBody(req, fileName)
        .then(function(){
            return simpleUploadMetadata(fileName, session_id, res)
        })
}

function simpleUploadMetadata(fn, session_id, res){
    var session = new Session(session_id);
    return Promise.resolve().then(function(){ // need this to bluebirdify Promise
        return parseMetadata(fn)
    }).then(function(metadata){
        // Convert the full paths in metadata to just the filenames
        metadata.lpc_file = path.parse(metadata.lpc_file).base;
        metadata.audio_file = path.parse(metadata.audio_file).base
        session.metadata = metadata;
        return session.updateField("metadata")
    }).then(function(success){
        res.status(success? 200 : 500).json({success: success}).end();
    }).catch(function(e){
        res.status(500).json({error: "Problem with upload: " + e}).end();
        console.log("Upload problem:", e);
    }).finally(function(){ // cleanup the upload/ folder
        fs.unlinkSync(fn);
    });
};

module.exports.upload = function(mime, uploadfn, req, res){
    var fileName = req.fileName ||
        uploadPath + (req.header("filename") || "temp.tmp");
    var session_id = getSessionId(req, res);
    if(!session_id) {
        return;
    }
    saveBody(req, fileName)
        .then(function(){
            return simpleUpload(session_id, fileName, uploadfn, res)
        });
};

function simpleUpload(session_id, fileName, uploadfn, res){
    var session = new Session(session_id);
    
    return Promise.resolve().then(function(){ // need this to bluebirdify Promise
        return session[uploadfn](fileName);
    }).then(function(success){
        res.status(success? 200 : 500).json({success: success}).end();
    }).catch(function(e){
        res.status(500).json({error: "Problem with upload: " + e}).end();
        console.log("Upload problem:", e);
    }).finally(function(){
        fs.unlinkSync(fileName);
    });
}

module.exports.uploadLPC = function(req, res){
    module.exports.upload("text/csv", "uploadLPC", req, res);
};
    
module.exports.uploadAudio = function(req, res){
    if(!/\.m4a$/.test(req.header("filename"))){
        req.fileName = req.header("filename") + ".m4a";
    }
    module.exports.upload("audio/mp4", "uploadAudio", req, res);
};

module.exports.uploadRatings = function(req, res){
    module.exports.upload("text/json", "uploadRatings", req, res);
};
